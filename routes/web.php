<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
    route::get('/uji', function () {
        return 'uji';
});
    route::get('name/{name}', function ($name) {
        return "name $name";
});
    route::get('/form', 'RegisterController@form');

    route::get('/sapa', 'RegisterController@sapa');

    route::get('/register', 'AuthController@register');

    route::post('/welcome', 'AuthController@welcome');

    // route::post('/welcome', 'AuthController@welcome_post');
    


 
